# [Visualizar Documentação Online](https://sige-gces2021-1.gitlab.io/equipe/) 

# Equipe

Repositório com o objetivo de disponibilizar a oganização da equipe no projeto [SIGE-UnB](https://gitlab.com/lappis-unb/projects/SMI) na disciplica [GCES 2021-1](https://github.com/FGA-GCES/A-disciplina) 

## Rode com Docker

Para executar localmente a aplicação, proceda com os seguintes passos:

1. Instale o Docker [neste link](https://docs.docker.com/install/linux/docker-ce/ubuntu/);
2. Instale o Docker Compose [neste link](https://docs.docker.com/compose/install/);
3. Na pasta raiz do projeto, inicialize: `sudo docker-compose up --build`;
4. Acesso no navegador: http://localhost:8008/.

Deploy: realizado com _GitLab CI_ a partir da _branch main_.