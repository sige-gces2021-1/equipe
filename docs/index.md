
# INÍCIO

!!! info "Sobre a página"
    Página destinada à disciplina de [Gerência de Configuração Evolução de Software(2021.1)](https://github.com/FGA-GCES/A-disciplina). Reserva-se a armazenar e apresentar a organização da equipe no projeto [Sistema de Gestão Energética (SIGE)](https://gitlab.com/lappis-unb/projects/SMI).

Código fonte do projeto: [GitLabe - Sige 2021.1](https://gitlab.com/sige-gces2021-1)

## EQUIPE

| Foto | Nome (Primeiro e Último) | Email | GitLab |
|:----:|:------------------------:|:-----:|:------:|
| <img style="border-radius: 50%;" src="https://secure.gravatar.com/avatar/a76233303af9b9ecdeddbdf61e912715?s=800&d=identicon" title="Gabriel Hussein" width="50" height="50" /> | Gabriel Hussein | gabrielhussein83@gmail.com | [GabrielHussein](https://gitlab.com/GabrielHussein) |
| <img style="border-radius: 50%;" src="https://secure.gravatar.com/avatar/12a9ee95730b165ebf64531c6fd178c2?s=800&d=identicon" title="Geovanne Santos" width="50" height="50" /> | Geovanne Santos | geovannessaraiva97@gmail.com | [saraivinha](https://gitlab.com/saraivinha) |
| <img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/2115822/avatar.png?width=400" title="Lieverton Silva" width="50" height="50" /> | Lieverton Silva | lievertom@gmail.com | [lievertom](https://gitlab.com/lievertom) |
| <img style="border-radius: 50%;" src="https://secure.gravatar.com/avatar/c46bcde6a028cff6a86def7ea633bfb0?s=800&d=identicon" title="Lucas Martins" width="50" height="50" /> | Lucas Martins | lmlucasmachadom@gmail.com | [lucasmmachado](https://gitlab.com/lucasmmachado) |
| <img style="border-radius: 50%;" src="https://gitlab.com/uploads/-/system/user/avatar/4574591/avatar.png?width=400" title="Lude Ribeiro" width="50" height="50" /> | Lude Ribeiro | ludeyuri07@gmail.com | [luderibeiro](https://gitlab.com/luderibeiro) |
| <img style="border-radius: 50%;" src="https://secure.gravatar.com/avatar/68aa11cfca28daeda27c06d355ae850e?s=800&d=identicon" title="Paulo Lopes" width="50" height="50" /> | Paulo Lopes | pv.paulovictor.vp@gmail.com | [Poulvilho](https://gitlab.com/Poulvilho) |
| <img style="border-radius: 50%;" src="https://secure.gravatar.com/avatar/add6c1d5f3414e710e85641c00524600?s=800&d=identicon" title="Thiago Gomes" width="50" height="50" /> | Thiago Gomes | thiago.luiz20000@gmail.com | [thiagomes](https://gitlab.com/thiagomes) |
