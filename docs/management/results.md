# Resumo dos resultados obtidos

## Tomadas de decisões

Como os membros tinham horários variados, não era possível a presença de todos os membros nas reuniões de planejamento. Dessa forma, algumas issues eram pegas durante a semana e comunicadas via Telegram para que não houvesse membros fazendo a mesma issue. Também ficava a cargo de cada membro atualizar os documentos de Sprint com os seus resultados.

## Eventuais dificuldades do projeto

- Arquitetura VueJS totalmente nova.
- Comunicação entre a equipe possível apenas via Telegram.
- Membros retirando a disciplina durante o semestre.

## Listagem dos números de Commits

###  Geovanne

| Repositório | Quantidade |
| ----- | ----- |
| [Equipe](https://gitlab.com/sige-gces2021-1/equipe)  | 6 | 
| [SIGE-Slave](https://gitlab.com/lappis-unb/projects/SMI/smi-slave) | 3 |
| [SIGE-Master](https://gitlab.com/sige-gces2021-1/smi-master) | 7 |
| [SIGE-Documentação](https://gitlab.com/sige-gces2021-1/docs) | 1 |

Total: 17 commits.

### Lieverton

| Repositório | Quantidade |
| ----- | ----- |
| [Equipe](https://gitlab.com/sige-gces2021-1/equipe)  | 20 | 
| [SIGE-Slave](https://gitlab.com/lappis-unb/projects/SMI/smi-slave) | 4 |
| [SIGE-Master](https://gitlab.com/sige-gces2021-1/smi-master) | 9 |
| [SIGE-Documentação](https://gitlab.com/sige-gces2021-1/docs) | 2 |
| [SIGE-Front](https://gitlab.com/lappis-unb/projects/SMI/smi-front) | 1 |
| [SIGE-Mobile](https://gitlab.com/lappis-unb/projects/SMI/smi-mobile) | 3 |

Total: 39 commits.

### Paulo

| Repositório | Quantidade |
| ----- | ----- |
| [Equipe](https://gitlab.com/sige-gces2021-1/equipe)  | 6 | 
| [SIGE-Slave](https://gitlab.com/lappis-unb/projects/SMI/smi-slave) | 1 |
| [SIGE-Front](https://gitlab.com/lappis-unb/projects/SMI/smi-front) | 3 |

Total: 10 commits.
