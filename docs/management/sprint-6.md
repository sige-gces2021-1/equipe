# Planejamento Sprint 6

## Duração da Sprint

**Data de início:** 08/10/2021  
**Data de término:** 21/10/2021  
**Duração:** 14 dias

## Sprint Backlog


### Dívidas Técnicas

- [#37 System of users and permissions ( recover password, confirm email, etc) - Paulo Lopes](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/37)
- [#65 Measurements data have to be ordered when saved - Geovanne](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/issues/65)

### Issues adicionadas

- [Corrigir a issue de documentação dos end points do sige-master](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/merge_requests/116)
- [Corrigir a issue de documentação dos end points do sige-slave](https://gitlab.com/lappis-unb/projects/SMI/smi-slave/-/merge_requests/88)

### Issues feitas(link do pull request)

- [#37 System of users and permissions ( recover password, confirm email, etc) - Paulo Lopes](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/merge_requests/98)
- [Corrigir a issue de documentação dos end points do sige-master - Geovanne e Lieverton](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/merge_requests/116)
- [Corrigir a issue de documentação dos end points do sige-slave - Geovanne e Lieverton](https://gitlab.com/lappis-unb/projects/SMI/smi-slave/-/merge_requests/88)
- Corrigir testes unitários do master - Geovanne e Lieverton.
- Corrigir testes unitários do slave - Geovanne e Lieverton.
- Criação de uma nova issue para correção das models pro banco de dados de teste - Geovanne e Lieverton.



### Dívidas Técnicas

- [#65 Measurements data have to be ordered when saved - Geovanne](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/issues/65)

