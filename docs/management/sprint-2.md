# Planejamento Sprint 2

## Duração da Sprint

**Data de início:** 13/08/2021  
**Data de término:** 26/08/2021  
**Duração:** 14 dias

## Objetivo da Sprint

Essa sprint terá como focos principais, documentar os endpoints das Api's.

## Sprint Backlog

### Issues adicionadas

- [#3 Documentar End-points do Slave - Geovanne Santos, Lude Ribeiro, Paulo Victor Lopes, Tiago Luiz](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/3)
- [#4 Documentar End-points do Master - Geovanne Santos, Lude Ribeiro, Paulo Victor Lopes](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/4)
- [#11 Documentação do Front Dashboard - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/11)

### Issues feitas(link do pull request)

- [#3 Documentar End-points do Slave - Geovanne Santos, Lude Ribeiro, Paulo Victor Lopes, Tiago Luiz](https://gitlab.com/lappis-unb/projects/SMI/smi-slave/-/merge_requests/88)
- [#4 Documentar End-points do Master - Geovanne Santos, Lude Ribeiro, Paulo Victor Lopes](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/merge_requests/116)

### Dívidas Técnicas

- [#11 Documentação do Front Dashboard - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/11)
- As issues do swagger não foram realizadas o PR pois o pipeline estava quebrando.
