# Planejamento Sprint 5

## Duração da Sprint

**Data de início:** 24/09/2021  
**Data de término:** 07/10/2021  
**Duração:** 14 dias

## Sprint Backlog

- Melhorar qualidade de Software aplicando as técnicas de SOLID e Clean Code - Todos os membros

### Dívidas Técnicas

- [#37 System of users and permissions ( recover password, confirm email, etc) - Paulo Lopes](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/37)

### Issues adicionadas

- [Improve clean code events - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/merge_requests/115)
- [#Aplicar clean code e princípios solid nas views de measurements - Geovanne](https://gitlab.com/sige-gces2021-1/smi-master/-/commit/0a36e161b8be0fb3dacc4101b7743055591d687f)


### Issues feitas(link do pull request)

- [Improve clean code events - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/merge_requests/115)

### Dívidas Técnicas

- [#37 System of users and permissions ( recover password, confirm email, etc) - Paulo Lopes](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/37)
- [#Aplicar clean code e princípios solid nas views de measurements - Geovanne](https://gitlab.com/sige-gces2021-1/smi-master/-/commit/0a36e161b8be0fb3dacc4101b7743055591d687f)
