# Planejamento Sprint 3

## Duração da Sprint

**Data de início:** 27/08/2021  
**Data de término:** 09/09/2021  
**Duração:** 14 dias

## Sprint Backlog

## Dívidas técnicas feitas

- [#11 Documentação do Front Dashboard - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/11)

### Issues adicionadas

- [#10 Documentação do Front - Arquitetura Quasar/Vue.js - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/10)
- [#13 Documentação do Mobile - Arquitetura Quasar/Vue.js - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/13)
- [#14 Corrigir o Pipeline de Deploy da Página no Gitlab.io - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/14)
- [#26 Atualizar vesão do node no app mobile - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-mobile/-/issues/26)
- [#27 Corrigir a ordenação da lista de trensdutores - Geovanne Santos, Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-mobile/-/issues/27)

### Issues feitas(link do pull request)

- [#10 Documentação do Front - Arquitetura Quasar/Vue.js - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/merge_requests/6)
- [#11 Documentação do Front - Dashboard - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/merge_requests/6)
- [#13 Documentação do Mobile - Arquitetura Quasar/Vue.js - Lucas Machado](https://gitlab.com/lappis-unb/projects/SMI/docs/-/merge_requests/7)
- [#14 Corrigir o Pipeline de Deploy da Página no Gitlab.io - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/docs/-/merge_requests/4)
- [#26 Atualizar vesão do node no app mobile - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-mobile/-/merge_requests/30)

### Dívidas Técnicas

- [#9 Documentação do Master - Coleta de dados Master-Slave - Lude Ribeiro](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/9)
- [#27 Corrigir a ordenação da lista de trensdutores - Geovane Santos, Lieverton Santos](https://gitlab.com/sige-gces2021-1/smi-master/-/tree/ordering-transductor)
- [#78 Events are not being closed - Lude Ribeiro](https://gitlab.com/lappis-unb/projects/SMI/smi-slave/-/issues/78)
