# Planejamento Sprint 1

## Duração da Sprint

**Data de início:** 03/08/2021  
**Data de término:** 12/08/2021  
**Duração:** 9 dias  

## Objetivo da Sprint

Essa sprint terá como focos principais, conhecer o produto mais a fundo, entender o contexto do sistema, estabeler contato com o mantenedor,
clonar e subir a aplicação, pegar issues de documentação.

## Sprint Backlog
- Estudar repositórios - Todos os membros
### Issues adicionadas

- [#8 Documentação do Master - Geovanne Santos](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/8)

### Issues feitas(link do pull request)

- [#8 Documentação do Master - Geovanne Santos](https://gitlab.com/lappis-unb/projects/SMI/docs/-/issues/8)

### Dívidas Técnicas

Não existem dívidas técnicas.
