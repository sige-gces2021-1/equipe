# Planejamento Sprint 4

## Duração da Sprint

**Data de início:** 10/09/2021  
**Data de término:** 23/09/2021  
**Duração:** 14 dias

## Sprint Backlog


### Dívidas Técnicas feitas

- [#27 Corrigir a ordenação da lista de trensdutores - Geovane Santos, Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-mobile/-/issues/27)

### Issues adicionadas

- [#37 System of users and permissions ( recover password, confirm email, etc) - Paulo Lopes](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/37)
- [#65 Measurements data have to be ordered when saved - Geovanne](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/issues/65)
- [#105 Atualizar vesão do node no front - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/105)

### Issues feitas(link do pull request)

- [#27 Corrigir a ordenação da lista de transdutores - Geovanne e Lieverton](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/merge_requests/114)
- [#105 Atualizar vesão do node no front - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/merge_requests/97)

### Dívidas Técnicas

- [#37 System of users and permissions ( recover password, confirm email, etc) - Paulo Lopes](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/37)
- [#65 Measurements data have to be ordered when saved - Geovanne](https://gitlab.com/lappis-unb/projects/SMI/smi-master/-/issues/65)
- [#105 Atualizar vesão do node no front - Lieverton Santos](https://gitlab.com/lappis-unb/projects/SMI/smi-front/-/issues/105)
